FROM microsoft/dotnet:2.2-sdk AS build
WORKDIR /src
COPY SnobSpider/ SnobSpider/
COPY Test/ Test/
WORKDIR /src/Test
RUN dotnet test 

WORKDIR /src/SnobSpider
RUN dotnet publish Pauk.SnobSpider.csproj -c Release -o /app

FROM microsoft/dotnet:2.2-runtime AS base
WORKDIR /app
COPY --from=build /app .
ENTRYPOINT ["dotnet", "Pauk.SnobSpider.dll"]
