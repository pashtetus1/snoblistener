﻿using System;
using System.IO;
using System.Net;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using FluentScheduler;
using HtmlAgilityPack;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Pauk.SnobSpider.Db;
using Pauk.SnobSpider.Domain;
using Pauk.SnobSpider.Infrastructure;
using Pauk.SnobSpider.Infrastructure.CheckBotStructure;
using Pauk.SnobSpider.Infrastructure.CheckJobStructure;
using Pauk.SnobSpider.Infrastructure.LinkGetter;
using Pauk.SnobSpider.Infrastructure.Sheduler;
using Pauk.SnobSpider.Infrastructure.TelegramBot;
using Telegram.Bot;

namespace Pauk.SnobSpider
{
    class Program
    {

        static async Task Main(string[] args)
        {
            var serviceCollection = new ServiceCollection();
            await ConfigureServices(serviceCollection);
            var serviceProvider = serviceCollection.BuildServiceProvider();
            var factory = serviceProvider.GetRequiredService<ILoggerFactory>();
            factory.AddConsole(LogLevel.Trace);

            var quitEvent = new ManualResetEvent(false);
            Console.CancelKeyPress += (o, e) =>
            {
                quitEvent.Set();
                e.Cancel = true;
            };
          
            
            var logger = serviceProvider.GetRequiredService<ILogger<Program>>();
            var bot = serviceProvider.GetService<ITelegramBot>();
          
            var sheduler = serviceProvider.GetRequiredService<Registry>();
            JobManager.Initialize(sheduler);
            await bot.Start();
            
            //Task.Factory.StartNew()
            logger.LogInformation("All services run");
            quitEvent.WaitOne();
            logger.LogInformation("Work end");
        }

        private static async Task ConfigureServices(ServiceCollection serviceCollection)
        {
            var configuration = new ConfigurationBuilder()
                .SetBasePath(Directory.GetCurrentDirectory())
                .AddJsonFile("appsettings.json", optional: false, reloadOnChange: true)
                .AddJsonFile($"appsettings.{Environment.GetEnvironmentVariable("ASPNETCORE_ENVIRONMENT")}.json", optional: true)
                .AddEnvironmentVariables()
                .Build();

            serviceCollection.AddLogging(x => x.AddConfiguration(configuration.GetSection("Logging")));
            var serviceProvider = serviceCollection.BuildServiceProvider();

            var factory = serviceProvider.GetRequiredService<ILoggerFactory>();
            factory.AddConsole(LogLevel.Trace);
            var logger = serviceProvider.GetRequiredService<ILogger<Program>>();

            var proxy = new WebProxy
            {
                Address = configuration.GetValue<Uri>("ProxyUrl")
            };
            serviceCollection.Configure<TelegramBotOptions>(
                x=>
                {
                    x.ExpirationTime = TimeSpan.FromDays(1);
                    x.AdminChatId = configuration.GetValue<int>("Telegram:AdminChatId");
                });
            serviceCollection.Configure<ShedulerOption>( options => configuration.GetSection("Sheduler").Bind(options) );
            serviceCollection.Configure<LinkGetterOption>(options => configuration.GetSection("Snob").Bind(options));
            serviceCollection.Configure<LiteDbConfig>(options => options.DatabasePath = Path.Join("db", "main.db"));

            Directory.CreateDirectory("db");
            serviceCollection.AddTransient<LiteDbContext>();
            serviceCollection.AddSingleton<ITelegramBot, TelegramBot>();
            serviceCollection.AddTransient<ICheckJob, CheckJob>();
            serviceCollection.AddTransient<ICheckBotJob, CheckBotJob>();
            serviceCollection.AddTransient<IDiagnostic, Diagnostic>();
            serviceCollection.AddTransient<ILinkGetter, ActivityLinkGet>();
            serviceCollection.AddTransient<IActivityContext, ActivityContext>();
            serviceCollection.AddSingleton<Registry, Sheduler>();
            serviceCollection.AddTransient<HtmlWeb>();
          
            var httpClient = new HttpClient
            {
                Timeout = TimeSpan.FromSeconds(5)
            };
            var token = configuration["Telegram:Token"];
            logger.LogInformation("Token {token}", token);
            var bot = await GenerateBot(httpClient, token, proxy, logger);
            serviceCollection.AddSingleton<ITelegramBotClient>(bot);
        }

        private static async Task<TelegramBotClient> GenerateBot(HttpClient httpClient, string token, WebProxy proxy, ILogger<Program> logger)
        {
            TelegramBotClient bot = null;
            try
            {
                await httpClient.GetAsync("https://telegram.org/");
            }
            catch (HttpRequestException)
            {
                bot = new TelegramBotClient(token, proxy);
                logger.LogInformation("Use proxy");
            }
            catch (OperationCanceledException)
            {
                bot = new TelegramBotClient(token, proxy);
                logger.LogInformation("Use proxy");
            }
            logger.LogInformation($"Enviroment {Environment.GetEnvironmentVariable("ASPNETCORE_ENVIRONMENT")}");
            
            if (bot != null) return bot;
            bot = new TelegramBotClient(token);
            logger.LogInformation("Bot no need proxy");

            return bot;
        }
    }
}
