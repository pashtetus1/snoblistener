﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using LiteDB;
using Microsoft.Extensions.Logging;
using Pauk.SnobSpider.Db;
using Pauk.SnobSpider.Model;

namespace Pauk.SnobSpider.Domain
{
    class ActivityContext : IActivityContext
    {
        private readonly ILogger<ActivityContext> _activiLogger;
        private readonly LiteDatabase _db;

        public ActivityContext(LiteDbContext db, ILogger<ActivityContext> activiLogger)
        {
            _activiLogger = activiLogger;
            _db = db.Context;
        }

        public string GetReport()
        {
            var builder = new StringBuilder();
            TimeZoneInfo timeZone;
            try
            {
                timeZone = TimeZoneInfo.FindSystemTimeZoneById("Europe/Moscow");
            }
            catch (TimeZoneNotFoundException e)
            {
                _activiLogger.LogWarning(e, "Europe/Moscow timezone not found");
                timeZone = TimeZoneInfo.Local;
            }
            foreach (var act in GetActivity().OrderByDescending(x => x.CreateDateTime))
            {
                var row = $"{act.Url} узнал {TimeZoneInfo.ConvertTime(act.CreateDateTime, timeZone):dd.MM HH:mm}\r\n";
                builder.Append(row);
            }

            return builder.ToString();
        }

        public IEnumerable<LinkActivity> GetActivity()
        {
            return _db.GetCollection<LinkActivity>().FindAll();
        }

        public IEnumerable<string> CheckNewLinkes(IEnumerable<string> currentLinkes)
        {
            var baseLinkesDb = _db.GetCollection<LinkActivity>();
            var historyLinkes = _db.GetCollection<HistoryLinkActivity>();

            foreach (var oldlink in baseLinkesDb.FindAll())
            {
                if (currentLinkes.Any(x => x == oldlink.Url))
                {
                    continue;
                }
                baseLinkesDb.Delete(x => x.Id == oldlink.Id);
                historyLinkes.Insert(
                    new HistoryLinkActivity
                    {
                        Url = oldlink.Url,
                        CreateDateTime = oldlink.CreateDateTime,
                        EndDate = DateTime.UtcNow
                    }
                );
            }

            var baseLinkes = baseLinkesDb.FindAll();

            var newLinkes = new List<string>();
            foreach (var currentLink in currentLinkes)
            {
                if (baseLinkes.Any(x => x.Url == currentLink))
                {
                    continue;
                }
                baseLinkesDb.Insert(
                    new LinkActivity
                    {
                        Url = currentLink,
                        CreateDateTime = DateTime.UtcNow
                    }
                );
                newLinkes.Add(currentLink);
            }

            return newLinkes;
        }
    }
}
