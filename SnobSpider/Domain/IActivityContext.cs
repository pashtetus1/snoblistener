﻿using System;
using System.Collections.Generic;
using System.Text;
using Pauk.SnobSpider.Model;

namespace Pauk.SnobSpider.Domain
{
    interface IActivityContext
    {
        string GetReport();
        IEnumerable<LinkActivity> GetActivity();
        IEnumerable<string> CheckNewLinkes(IEnumerable<string> currentLinkes);
    }
}
