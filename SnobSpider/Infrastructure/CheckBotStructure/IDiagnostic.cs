﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Pauk.SnobSpider.Infrastructure.CheckBotStructure
{
    interface IDiagnostic
    {
        string Do();
    }
}
