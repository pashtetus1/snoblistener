﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Pauk.SnobSpider.Domain;
using Pauk.SnobSpider.Infrastructure.LinkGetter;

namespace Pauk.SnobSpider.Infrastructure.CheckBotStructure
{
    class Diagnostic : IDiagnostic
    {
        private readonly IActivityContext _activityContext;
        private readonly ILinkGetter _linkGetter;

        public Diagnostic(IActivityContext activityContext, ILinkGetter linkGetter)
        {
            _activityContext = activityContext;
            _linkGetter = linkGetter;
        }

        public string Do()
        {
            var snoblinks = _linkGetter.Get().Count();
            var localLinks = _activityContext.GetActivity().Count();
            var message = $"Я на связи, у сноба {snoblinks} ссылок локально {localLinks}";
            return message;
        }
    }
}
