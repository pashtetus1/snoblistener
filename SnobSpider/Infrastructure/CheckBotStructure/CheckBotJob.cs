﻿using System;
using System.Linq;
using Microsoft.Extensions.Logging;
using Pauk.SnobSpider.Domain;
using Pauk.SnobSpider.Infrastructure.LinkGetter;
using Pauk.SnobSpider.Infrastructure.TelegramBot;
using Telegram.Bot;

namespace Pauk.SnobSpider.Infrastructure.CheckBotStructure
{
    class CheckBotJob : ICheckBotJob
    {
        private readonly ITelegramBot _botClient;
        private readonly IDiagnostic _diagnostic;
        private readonly ILogger<CheckBotJob> _logger;

        public CheckBotJob(ITelegramBot botClient, IDiagnostic diagnostic, ILogger<CheckBotJob> logger)
        {
            _botClient = botClient;
            _diagnostic = diagnostic;
            _logger = logger;
        }

        public void Execute()
        {
            try
            {
                _botClient.SendAdmin(_diagnostic.Do());
            }
            catch (Exception e)
            {
                _logger.LogError(e, "problem when diagnostic job");
            }
          
        }
    }
}