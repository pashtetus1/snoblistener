﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using HtmlAgilityPack;
using LiteDB;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Pauk.SnobSpider.Db;
using Pauk.SnobSpider.Domain;
using Pauk.SnobSpider.Infrastructure.LinkGetter;
using Pauk.SnobSpider.Model;
using Telegram.Bot;

namespace Pauk.SnobSpider.Infrastructure.CheckJobStructure
{
    class CheckJob: ICheckJob
    {
        private readonly LiteDatabase _db;
        private readonly ITelegramBotClient _telegramBot;
        private readonly ILinkGetter _linkGetter;
        private readonly IActivityContext _activityContext;
        private readonly ILogger<CheckJob> _logger;
        private int _lastSendingMessagesCount = 0;
        private DateTime _lastStartTime;

        public CheckJob(LiteDbContext db, ITelegramBotClient telegramBot, ILinkGetter linkGetter, IActivityContext activityContext, ILogger<CheckJob> logger)
        {
            _db = db.Context;
            _telegramBot = telegramBot;
            _linkGetter = linkGetter;
            _activityContext = activityContext;
            _logger = logger;
        }

        public void Execute()
        {
            try
            {
                Check().GetAwaiter().GetResult();
            }
            catch (Exception exception)
            {
                _logger.LogError(exception, "error when check");
            }
        }

        private async Task Check()
        {
           
            var linkes = _linkGetter.Get();
            var newLinkes = _activityContext.CheckNewLinkes(linkes);

            if (newLinkes.Any())
            {
                _lastStartTime = DateTime.UtcNow;
                _logger.LogInformation("new {count} linkes", newLinkes.Count());
                var activeChatIds = GetAllActiveSubscribtionChatId();
                foreach (var chatId in activeChatIds)
                {
                    await SendLinksToUser(newLinkes, chatId);
                }
            }
        }

        private async Task SendLinksToUser(IEnumerable<string> newLinkes, int chatId)
        {
            try
            {
                foreach (var url in newLinkes)
                {
                    if (_lastSendingMessagesCount > 25)
                    {
                        var now = DateTime.UtcNow;
                        if (now - _lastStartTime <= TimeSpan.FromSeconds(1))
                        {
                            await Task.Delay(1000);
                            _logger.LogWarning("heavy load", chatId);
                        }
                        _lastStartTime = now;
                        _lastSendingMessagesCount = 0;
                    }
                    await _telegramBot.SendTextMessageAsync(chatId, url);
                    _lastSendingMessagesCount++;
                }
            }
            catch (Telegram.Bot.Exceptions.ApiRequestException e)
            {
                if (e.ErrorCode == 403)
                {
                    _logger.LogWarning("user {user} block messages", chatId);
                    BlockSubscription(chatId);
                }
            }
        }

        private void BlockSubscription(int chatId)
        {
            var subContext = _db.GetCollection<Subscription>();
            var Subscription = subContext.Include(x => x.User).FindAll().FirstOrDefault(x => x.User.TelegramId == chatId);
            Subscription.State = SubscriptionState.Blocked;
            Subscription.ChangeDate = DateTime.UtcNow;
            subContext.Update(Subscription);
        }

        private IEnumerable<int> GetAllActiveSubscribtionChatId()
        {
            return _db.GetCollection<Subscription>().Include(x => x.User).FindAll()
                .Where(x => x.State == SubscriptionState.Active)
                .Select(x => x.User.TelegramId);

        }
    }
}
