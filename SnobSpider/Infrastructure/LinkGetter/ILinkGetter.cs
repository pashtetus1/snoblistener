﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Pauk.SnobSpider.Infrastructure.LinkGetter
{
    interface ILinkGetter
    {
        IEnumerable<string> Get();
    }
}
