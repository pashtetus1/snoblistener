﻿using System;

namespace Pauk.SnobSpider.Infrastructure.LinkGetter
{
    class LinkGetterOption
    {
        public Uri Url { get; set; }
    }
}
