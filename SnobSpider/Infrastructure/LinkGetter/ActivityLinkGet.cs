﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using HtmlAgilityPack;
using Microsoft.Extensions.Options;
using Pauk.SnobSpider.Infrastructure.CheckJobStructure;

namespace Pauk.SnobSpider.Infrastructure.LinkGetter
{
    class ActivityLinkGet: ILinkGetter
    {
        private readonly HtmlWeb _htmlWeb;
        private readonly IOptions<LinkGetterOption> _options;

        public ActivityLinkGet(HtmlWeb htmlWeb, IOptions<LinkGetterOption> options)
        {
            _htmlWeb = htmlWeb;
            _options = options;
        }

        public IEnumerable<string> Get()
        {
            var uri = _options.Value.Url;
            var doc = _htmlWeb.Load(uri);

            return doc.DocumentNode
                .SelectNodes("//a[@href]").Where(x => x.Attributes["href"].Value.Contains("entry"))
                .Select(x => uri.Host + x.Attributes["href"].Value).Distinct();
        }
    }
}
