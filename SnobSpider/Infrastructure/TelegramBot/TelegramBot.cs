﻿using System;
using System.Linq;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using LiteDB;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Pauk.SnobSpider.Db;
using Pauk.SnobSpider.Domain;
using Pauk.SnobSpider.Infrastructure.CheckBotStructure;
using Pauk.SnobSpider.Infrastructure.CheckJobStructure;
using Pauk.SnobSpider.Model;
using Telegram.Bot;
using Telegram.Bot.Args;
using Telegram.Bot.Types;
using Telegram.Bot.Types.Enums;

namespace Pauk.SnobSpider.Infrastructure.TelegramBot
{
    internal class TelegramBot : ITelegramBot
    {
        private const string StartCommand = "/start";
        private const string ShareCommand = "/share";
        private const string StopCommand = "/stop";
        private const string ReportCommand = "/report";
        private const string CheckCommand = "/check";
        private const string CurrentActivityCommand = "/current";
        private const int RegisterLinkLength = 43;
        private readonly ITelegramBotClient _client;
        private readonly IActivityContext _activityContextContext;
        private readonly LiteDatabase _db;
        private readonly ILogger<TelegramBot> _logger;
        private readonly IDiagnostic _diagnostic;
        private readonly TelegramBotOptions _options;
        private User _botInfo;

        public TelegramBot(ITelegramBotClient client, LiteDbContext db, IActivityContext activityContextContext, IDiagnostic diagnostic, ILogger<TelegramBot> logger, IOptions<TelegramBotOptions> options)
        {
            _client = client;
            _activityContextContext = activityContextContext;
            _db = db.Context;
            _logger = logger;
            _diagnostic = diagnostic;
            _options = options.Value;
        }

        public async Task Start()
        {
            _client.OnMessage += Bot_OnMessage;
            _client.OnReceiveError += _client_OnReceiveError;
            _client.OnReceiveGeneralError += _client_OnReceiveGeneralError;
            _botInfo = await _client.GetMeAsync();
            try
            {
                await _client.SendTextMessageAsync(_options.AdminChatId, "Я готов, Хозяин!");
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
            _logger.LogInformation("Bot {botName} start listen",_botInfo.Username);
            _client.StartReceiving(new[]
            {
                UpdateType.Message
            });
           
        }

        public async Task SendAdmin(string message)
        {
            await _client.SendTextMessageAsync(_options.AdminChatId, message);
        }

        private void _client_OnReceiveGeneralError(object sender, ReceiveGeneralErrorEventArgs e)
        {
            _logger.LogError(e.Exception, "telegram general error");
        }

        private void _client_OnReceiveError(object sender, ReceiveErrorEventArgs e)
        {
           _logger.LogError(e.ApiRequestException, "telegram api error");
        }

        private async void Bot_OnMessage(object sender, MessageEventArgs e)
        {
            try
            {
                var message = e.Message;
                await ProccessMessage(message);
            }
            catch (Exception exception)
            {
               _logger.LogError(exception, "Error when receive message {message}", e.Message.Text);
            }
            
        }

        private async Task ProccessMessage(Message message)
        {
            var entityValues = message.EntityValues;
            var user = message.From;
            var userId = user.Id;
            _logger.LogInformation("Receive message {message} from {userId}", message.Text, userId);
            var isRegister = UserIsRegister(userId);
            var isAdmin = userId == _options.AdminChatId;
            var messageText = message.Text;
            if (isRegister)
            {
                if (entityValues != null)
                {
                    if (entityValues.Contains(StartCommand))
                    {
                        _logger.LogWarning("User try reregister {message} {id}", messageText, userId);
                        TryRenewSubscription(userId);
                        await _client.SendTextMessageAsync(userId, TeleMessages.YouAlreadyRegister);
                    }
                    else if (entityValues.Contains(ShareCommand))
                    {
                        var key = GenerateKey(userId);
                        await _client.SendTextMessageAsync(userId, "Поделитесь этой ссылкой с товарищем");
                        await _client.SendTextMessageAsync(userId, $"tg://resolve?domain={_botInfo.Username}&start={key}");
                    }
                    else if (entityValues.Contains(StopCommand))
                    {
                        StopSubscription(userId);
                        await _client.SendTextMessageAsync(userId, TeleMessages.StopSubscribtion);
                    }
                    else if (entityValues.Contains(CurrentActivityCommand))
                    {
                        var report = _activityContextContext.GetReport();
                        await _client.SendTextMessageAsync(userId, report);
                    }
                    else if (isAdmin)
                    {
                        if (entityValues.Contains(ReportCommand))
                        {
                            var report = GenerateReport();
                            await _client.SendTextMessageAsync(userId, report);
                        }
                        else if (entityValues.Contains(CheckCommand))
                        {
                            await SendAdmin(_diagnostic.Do());
                        }
                    }

                }
                else
                {
                    await _client.SendTextMessageAsync(userId, "Приветик!");
                }
            }
            else
            {
                if (entityValues != null && entityValues.Contains(StartCommand))
                {
                    var newUser = new SpyUser
                    {
                        NickName = user.Username,
                        TelegramId = userId,
                        Created = DateTime.UtcNow,
                        FirstName = user.FirstName,
                        LastName = user.LastName,
                        LangCode = user.LanguageCode,

                    };
                    if (messageText.Length == RegisterLinkLength)
                    {
                        var guidString = messageText.Substring(7);
                        var goodGuid = Guid.TryParse(guidString, out var guidResult);
                        if (goodGuid)
                        {
                            var result = RegisterNew(newUser, guidResult);
                            if (result)
                            {
                                await _client.SendTextMessageAsync(userId, TeleMessages.UserSuccessRegister);
                                await SendAdmin($"Зарегался новый юзер {userId} {user.Username}");
                            }
                            else
                            {
                                GenerateStrangeUserWarning(userId, user, messageText);
                            }
                        }
                        else
                        {
                            GenerateStrangeUserWarning(userId, user, messageText);
                        }
                    }
                    else if (isAdmin)
                    {
                        RegisterNewAdmin(newUser);
                        await _client.SendTextMessageAsync(userId, TeleMessages.AdminSuccessRegister);
                    }
                    else
                    {
                        GenerateStrangeUserWarning(userId, user, messageText);
                    }
                }
                else
                {
                    _logger.LogInformation("Not registered user {id} {username} send message {message}", userId, user.Username,
                        messageText);
                }
            }
        }

        private string GenerateReport()
        {
            var subscriptions = _db.GetCollection<Subscription>().Include(x => x.User).FindAll();
            var builder = new StringBuilder();
            foreach (var subscription in subscriptions)
            {
                var user = subscription.User;
                var row =
                    $"{subscription.User.TelegramId} {subscription.User.Generation} {subscription.State} " +
                    $"{user.Created} {subscription.ChangeDate} {user.NickName} {user.FirstName} {user.LastName} {user.LangCode}\r\n";
                builder.Append(row);
            }

            return builder.ToString();
        }

        private void StopSubscription(int userId)
        {
            var subContext = _db.GetCollection<Subscription>();
            var Subscription = subContext.Include(x => x.User).FindAll().FirstOrDefault(x => x.User.TelegramId == userId);
            if (Subscription.State == SubscriptionState.Stoped) return;
            _logger.LogInformation("user change subState from {old} to Stop", Subscription.State.ToString());
            Subscription.State = SubscriptionState.Stoped;
            Subscription.ChangeDate = DateTime.UtcNow;
            subContext.Update(Subscription);
        }

        private void TryRenewSubscription(int userId)
        {
            var subContext = _db.GetCollection<Subscription>();
            var Subscription = subContext.Include(x => x.User).FindAll().FirstOrDefault(x => x.User.TelegramId == userId);
            if (Subscription.State == SubscriptionState.Active) return;
            _logger.LogInformation("user change subState from {old} to Active", Subscription.State.ToString());
            Subscription.State = SubscriptionState.Active;
            Subscription.ChangeDate = DateTime.UtcNow;
            subContext.Update(Subscription);
        }

        private void GenerateStrangeUserWarning(int userId, User user, string messageText)
        {
            _logger.LogWarning("Not registered user {id} {username} send message strange {message}", userId, user.Username,
                messageText);
        }

        private Guid GenerateKey (int userId)
        {
            var newGuid = Guid.NewGuid();
            var now = DateTime.UtcNow;
            var user = GetUser(userId);
            var newKey = new RegKey
            {
                Author = user,
                Created = now,
                Value = newGuid,
                ExpirationDate = now.Add(_options.ExpirationTime)
            };
            var regKey = _db.GetCollection<RegKey>();
            regKey.Insert(newKey);
            return newGuid;
        }

        private bool RegisterNew(SpyUser newUser, Guid keyGuid)
        {
            var now = DateTime.UtcNow;
            var keys = _db.GetCollection<RegKey>();
            var validKey = keys.Include(x => x.Author).FindOne(x => 
                x.Value == keyGuid && 
                x.ExpirationDate >= now && 
                x.RegisteredUser == null
                );
            if (validKey == null)
            {
                return false;
            }

            var users = _db.GetCollection<SpyUser>();
            newUser.Generation = validKey.Author.Generation + 1;

            users.Insert(newUser);

            validKey.RegisteredUser = newUser;
            keys.Update(validKey);

            var newSubscription = new Subscription
            {
                State = SubscriptionState.Active,
                User = newUser,
                ChangeDate = DateTime.UtcNow
            };
            var subscriptions = _db.GetCollection<Subscription>();
            subscriptions.Insert(newSubscription);
            _logger.LogInformation("Register new user {id} {nick}", newUser.TelegramId, newUser.NickName);
            return true;
        }

        private void RegisterNewAdmin(SpyUser newUser)
        {
            var users = _db.GetCollection<SpyUser>();
            newUser.Generation = 0;

            users.Insert(newUser);

            var newSubscription = new Subscription
            {
                State = SubscriptionState.Active,
                User = newUser,
                ChangeDate = DateTime.UtcNow
            };
            var subscriptions = _db.GetCollection<Subscription>();
            subscriptions.Insert(newSubscription);

            _logger.LogInformation("Register new admin {id}", newUser.TelegramId, newUser.NickName);
        }

        private bool UserIsRegister(int telegramChatId)
        {
            var users = _db.GetCollection<SpyUser>();
            return users.Exists(x => x.TelegramId == telegramChatId);
        }

        private SpyUser GetUser(int telegramChatId)
        {
            var users = _db.GetCollection<SpyUser>();
            return users.FindOne(x => x.TelegramId == telegramChatId);
        }
    }
}