﻿using System.Threading.Tasks;

namespace Pauk.SnobSpider.Infrastructure.TelegramBot
{
    internal interface ITelegramBot
    {
        Task Start();
        Task SendAdmin(string message);
    }
}