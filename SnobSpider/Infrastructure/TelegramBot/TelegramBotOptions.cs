﻿using System;

namespace Pauk.SnobSpider.Infrastructure.TelegramBot
{
    class TelegramBotOptions
    {
        public int AdminChatId { get; set; }
        public TimeSpan ExpirationTime { get; set; }
    }
}
