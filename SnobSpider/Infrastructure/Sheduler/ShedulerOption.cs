﻿using System;

namespace Pauk.SnobSpider.Infrastructure.Sheduler
{
    class ShedulerOption
    {
        public TimeSpan CheckSiteInterval { get; set; }
    }
}
