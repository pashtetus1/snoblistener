﻿using FluentScheduler;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Pauk.SnobSpider.Infrastructure.CheckBotStructure;
using Pauk.SnobSpider.Infrastructure.CheckJobStructure;

namespace Pauk.SnobSpider.Infrastructure.Sheduler
{
    class Sheduler : Registry
    {
        public Sheduler(ICheckJob checkJob, ICheckBotJob checkBotJob, ILogger<Sheduler> logger, IOptions<ShedulerOption> options)
        {
            var interval = options.Value.CheckSiteInterval;
            NonReentrantAsDefault();
            Schedule(() => checkJob).ToRunNow().AndEvery((int)interval.TotalMilliseconds).Milliseconds();
            Schedule(() => checkBotJob).ToRunEvery(1).Days().At(12,00);
            logger.LogInformation("Site will be check every {interval}", interval);
        }
    }
}
