﻿namespace Pauk.SnobSpider.Db
{
    public class LiteDbConfig
    {
        public string DatabasePath { get; set; }
        public bool InMemory { get; set; }
    }
}