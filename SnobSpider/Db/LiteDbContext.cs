﻿using System;
using System.IO;
using LiteDB;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;

namespace Pauk.SnobSpider.Db
{
    public class LiteDbContext: IDisposable
    {
        public readonly LiteDatabase Context;
        public LiteDbContext(ILogger<LiteDbContext> logger, IOptions<LiteDbConfig> option)
        {
            var config = option.Value;
            LiteDatabase db;
            if (config.InMemory ^ string.IsNullOrEmpty(config.DatabasePath))
            {
                throw new ArgumentException("Or inMemory or exist DatabasePath");
            }

            if (config.InMemory)
            {
                db = new LiteDatabase(new MemoryStream());
            }
            else
            {
                var litedbLogger = new Logger(Logger.COMMAND | Logger.ERROR, x => logger.LogTrace(x));
                db = new LiteDatabase(option.Value.DatabasePath, log: litedbLogger);
            }

           
            Context = db;
        }

        public void Dispose()
        {
            Context.Dispose();
        }
    }
}