﻿using System;

namespace Pauk.SnobSpider.Model
{
    class LinkActivity
    {
        public int Id { get; set; }
        public string Url { get; set; }
        public DateTime CreateDateTime { get; set; }
    }
}
