﻿using System;
using LiteDB;

namespace Pauk.SnobSpider.Model
{
    class Subscription
    {
        public int Id { get; set; }
        [BsonRef("SpyUser")]
        public SpyUser User { get; set; }
        public SubscriptionState State { get; set; }
        public DateTime ChangeDate { get; set; }
    }
}
