﻿using System;

namespace Pauk.SnobSpider.Model
{
    class HistoryLinkActivity : LinkActivity
    {
        public DateTime EndDate { get; set; }
    }
}
