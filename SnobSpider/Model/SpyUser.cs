﻿using System;

namespace Pauk.SnobSpider.Model
{
    class SpyUser
    {
        public int Id { get; set; }
        public int TelegramId { get; set; }
        public int Generation { get; set; }
        public string NickName { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string LangCode { get; set; }
        public DateTime Created { get; set; }
    }
}
