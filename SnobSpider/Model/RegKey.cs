﻿using System;
using LiteDB;

namespace Pauk.SnobSpider.Model
{
    class RegKey
    {
        public int Id { get; set; }
        public Guid Value { get; set; }
        [BsonRef("SpyUser")]
        public SpyUser Author { get; set; }
        [BsonRef("SpyUser")]
        public SpyUser RegisteredUser { get; set; }
        public DateTime Created { get; set; }
        public DateTime ExpirationDate { get; set; }
    }
}
