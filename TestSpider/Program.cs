﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using HtmlAgilityPack;
using LiteDB;
using Telegram.Bot;
using Telegram.Bot.Args;
using Telegram.Bot.Types.Enums;

namespace Pauk.TestSpider
{
    public class Order
    {
        public ObjectId Id { get; set; }
        public DateTime OrderDate { get; set; }
        public Customer Customer { get; set; }
        public List<Product> Products { get; set; }
    }

    public class Product
    {
        public int Id { get; set; }
        public string Description { get; set; }
        public double Price { get; set; }
    }

    class Program
    {
        private static void Main(string[] args)
        {
            Console.WriteLine("Start!");
            //await TCPListener();
            var a = TimeZoneInfo.Local;
            var db = new LiteDatabase(new MemoryStream());
            var customers = db.GetCollection("customers");
            var products = db.GetCollection("products");
            var orders = db.GetCollection("orders");

            //TelegramTest();
            //DbTest();
            //HtmlAgilityPack();
            Console.ReadLine();
        }

        private static async Task TCPListener()
        {
            var Listener = new TcpListener(IPAddress.Any, 7000);
            Listener.Start();
          
            while (true)
            {
                var result = await Listener.AcceptTcpClientAsync();
                NetworkStream stream = result.GetStream();
                byte[] data = new byte[1024];
                stream.Read(data, 0, data.Length);

                var str = Encoding.ASCII.GetString(data, 0, (int) data.Length);
                var nameValueCollection = HttpUtility.ParseQueryString(str);
                //string str;
                //byte[] data = new byte[1024];
                //using (MemoryStream ms = new MemoryStream())
                //{

                //    int numBytesRead;
                //    while ((numBytesRead = stream.Read(data, 0, data.Length)) > 0)
                //    {
                //        ms.Write(data, 0, numBytesRead);


                //    }

                //}
                var address = result.Client.LocalEndPoint.Serialize();
                Console.WriteLine(result);
            }
        }

        private static void DbTest()
        {
            using (var db = new LiteDatabase(@"MyData.db"))
            {
                // Produts and Customer are other collections (not embedded document)
                var customers = db.GetCollection<Customer>();
                var products = db.GetCollection<Product>();
                var orders = db.GetCollection<Order>();

                // create examples
                var john = new Customer {Name = "John Doe"};
                var tv = new Product {Description = "TV Sony 44\"", Price = 799};
                var iphone = new Product {Description = "iPhone X", Price = 999};
                var order1 = new Order
                    {OrderDate = new DateTime(2017, 1, 1), Customer = john, Products = new List<Product>() {iphone, tv}};
                var order2 = new Order
                    {OrderDate = new DateTime(2017, 10, 1), Customer = john, Products = new List<Product>() {iphone}};

                // insert into collections
                customers.Insert(john);
                products.Insert(new Product[] {tv, iphone});
                orders.Insert(new Order[] {order1, order2});

                // create index in OrderDate
                orders.EnsureIndex(x => x.OrderDate);

                // When query Order, includes references
                var query = orders
                    .Include(x => x.Customer)
                    .Include(x => x.Products)
                    .Find(x => x.OrderDate == new DateTime(2017, 1, 1));

                // Each instance of Order will load Customer/Products references
                foreach (var c in query)
                {
                    Console.WriteLine("#{0} - {1}", c.Id, c.Customer.Name);

                    foreach (var p in c.Products)
                    {
                        Console.WriteLine(" > {0} - {1:c}", p.Description, p.Price);
                    }
                }

                // Each instance of Order will load Customer/Products references
            }
        }

        private static void TelegramTest()
        {
            var proxy = new WebProxy
            {
                Address = new Uri("http://178.62.32.8:3128")
            };
            var httpClientHandler = new HttpClientHandler
            {
                Proxy = proxy,
                //SslProtocols = SslProtocols.Tls12
            };

            var httpclient = new HttpClient(httpClientHandler);

            var result = httpclient.GetAsync("https://telegram.org/").GetAwaiter().GetResult();
            var bot = new TelegramBotClient("764904365:AAFxA0jrr6vFEAHQ0HSC2ClStrVqhio7_Vg", proxy);
            var a = bot.GetMeAsync().GetAwaiter().GetResult();
            bot.OnMessage += Bot_OnMessage;
            bot.StartReceiving(new[]
            {
                UpdateType.Message
            });
           
        }

        private static void Bot_OnMessage(object sender, MessageEventArgs e)
        {
            Console.WriteLine(e.Message.Text);
        }

        private static void HtmlAgilityPack()
        {
            var url = "https://snob.ru/theme/171";
            var web = new HtmlWeb();
            var doc = web.Load(url);

            var nodes = doc.DocumentNode
                .SelectNodes("//a[@href]").Where(x => x.Attributes["href"].Value.Contains("entry"))
                .Select(x => x.Attributes["href"].Value).Distinct();
            Console.WriteLine(nodes);
        }
    }
}
