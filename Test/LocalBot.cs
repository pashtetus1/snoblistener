﻿using System;
using System.Reflection;
using System.Threading;
using System.Threading.Tasks;
using Moq;
using Telegram.Bot;
using Telegram.Bot.Args;
using Telegram.Bot.Types;
using Telegram.Bot.Types.Enums;
using Telegram.Bot.Types.ReplyMarkups;

namespace Pauk.SnobSpider.Test
{
    internal class LocalBot
    {
        public Action<long, string> ProcessReceiveMessage = (l, s) => { };
        private readonly Mock<ITelegramBotClient> _botMock;

        public LocalBot()
        {
            _botMock = new Mock<ITelegramBotClient>();
            _botMock.Setup(x => x.SendTextMessageAsync(It.IsAny<ChatId>(), It.IsAny<string>(), ParseMode.Default, false,
                false, 0, null, It.IsAny<CancellationToken>())).Returns<ChatId, string, ParseMode, bool, bool, int, IReplyMarkup, CancellationToken>(
                (chatId, message, x0, x1, x2, x3, x4, x5) =>
                {
                    ProcessReceiveMessage(chatId.Identifier, message);
                    return Task.FromResult(
                        new Message()
                    );
                }
            );
            _botMock.Setup(x => x.GetMeAsync(It.IsAny<CancellationToken>())).Returns<CancellationToken>((x) => Task.FromResult(new User()
            {
                IsBot = true
            }));
        }

        public ITelegramBotClient GetClient()
        {
            return _botMock.Object;
        }

        public void SendMessage(int chatId, string message)
        {
            var throwingMessage = new Message()
            {
                From = new User
                {
                    Id = chatId
                },
                Text = message
            };

            if (message[0] == '/')
            {
                var command = message.Split(' ');
                throwingMessage.Entities = new[] {new MessageEntity {Offset = 0, Length = command[0].Length}};
            }

            
            _botMock.Raise(x => x.OnMessage += null, Construct(throwingMessage));
        }

        public static MessageEventArgs Construct(Message param)
        {
            Type t = typeof(MessageEventArgs);

            ConstructorInfo ci = t.GetConstructor(
                BindingFlags.Instance | BindingFlags.NonPublic,
                null, new[] { typeof(Message) }, null);

            return (MessageEventArgs)ci.Invoke(new object[] { param });
        }
    }
}
