using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using HtmlAgilityPack;
using Microsoft.Extensions.DependencyInjection;
using Pauk.SnobSpider.Db;
using Pauk.SnobSpider.Domain;
using Pauk.SnobSpider.Infrastructure.CheckBotStructure;
using Pauk.SnobSpider.Infrastructure.CheckJobStructure;
using Pauk.SnobSpider.Infrastructure.LinkGetter;
using Pauk.SnobSpider.Infrastructure.Sheduler;
using Pauk.SnobSpider.Infrastructure.TelegramBot;
using Pauk.SnobSpider.Model;
using Xunit;
namespace Pauk.SnobSpider.Test
{
    public class TestSpider
    {
        private readonly int _adminId;
        private readonly SimpleServer _localSnobServer;
        private readonly Random _random = new Random();
        private readonly LocalBot _localBot;
        private readonly ServiceProvider _serviceProvider;
        private readonly ICheckJob _checkJob;

        public TestSpider()
        {
            _localSnobServer = new SimpleServer();
            _adminId = _random.Next(int.MaxValue);
            var serviceCollection = new ServiceCollection();
            serviceCollection.Configure<TelegramBotOptions>(
                x =>
                {
                    x.ExpirationTime = TimeSpan.FromDays(1);
                    x.AdminChatId = _adminId;
                });
            serviceCollection.Configure<LinkGetterOption>(options =>
            {
                options.Url = new Uri(_localSnobServer.ServerUrl);
            });

            serviceCollection.Configure<LiteDbConfig>(options => options.InMemory = true);
            serviceCollection.AddScoped<LiteDbContext>();
            serviceCollection.AddSingleton<ITelegramBot, TelegramBot>();
            serviceCollection.AddTransient<ILinkGetter, ActivityLinkGet>();
            serviceCollection.AddTransient<ICheckJob, CheckJob>();
            serviceCollection.AddTransient<IDiagnostic, Diagnostic>();
            serviceCollection.AddTransient<ICheckBotJob, CheckBotJob>();
            serviceCollection.AddTransient<IActivityContext, ActivityContext>();
            serviceCollection.AddTransient<HtmlWeb>();
            _localBot = new LocalBot();
            serviceCollection.AddSingleton(_localBot.GetClient());
            serviceCollection.AddLogging();
            _serviceProvider = serviceCollection.BuildServiceProvider();


            var botService = _serviceProvider.GetRequiredService<ITelegramBot>();
            botService.Start();
            _localSnobServer.Start();
            _checkJob = _serviceProvider.GetRequiredService<ICheckJob>();
        }

        [Fact]
        public void SpiderMustNotifyAboutLinks()
        {
            var link = "/selected/entry/145991";
            var messageWas = false;

            _localBot.ProcessReceiveMessage = (id, message) =>
            {
                Assert.Contains(link, message);
                Assert.Equal(id, _adminId);
                messageWas = true;
            };

            _localSnobServer.Answer = uri => $"<a href=\"{link}\">";


            var db = _serviceProvider.GetRequiredService<LiteDbContext>().Context;
            var userContext = db.GetCollection<SpyUser>();
            var user = new SpyUser
            {
                TelegramId = _adminId,
            };
            userContext.Insert(user);
            var subContext = db.GetCollection<Subscription>();
            subContext.Insert(new Subscription
            {
                State = SubscriptionState.Active,
                User = user
            });

            _checkJob.Execute();

            Assert.True(messageWas);
        }


        [Fact]
        public void BotMustRegisterAdminUser()
        {
            var messageWas = false;
            _localBot.ProcessReceiveMessage = (id, message) =>
            {
                Assert.Contains(TeleMessages.AdminSuccessRegister, message);
                messageWas = true;
            };

            _localBot.SendMessage(_adminId, "/start");
            var db = _serviceProvider.GetRequiredService<LiteDbContext>().Context;
            var subContext = db.GetCollection<Subscription>();
            Assert.True(subContext.Include(x => x.User).FindAll().Any(x => x.User.TelegramId == _adminId && x.State == SubscriptionState.Active), "No admin in database");
            Assert.True(messageWas);
        }

        [Fact]
        public void AdminCanRegisterNewUser()
        {
            _localBot.SendMessage(_adminId, "/start");

            var key = "";
            _localBot.ProcessReceiveMessage = (id, message) =>
            {
                if (!message.Contains("tg"))
                {
                    return;
                }
                var guidLength = Guid.Empty.ToString().Length;
                key = message.Substring(message.Length - guidLength, guidLength);
            };
            _localBot.SendMessage(_adminId, "/share");
            _localBot.ProcessReceiveMessage = (id, message) =>
            {
                
            };
            var newSimpleUserId = _adminId + 1;
            _localBot.ProcessReceiveMessage = (id, message) =>
            {
                Assert.Contains(TeleMessages.UserSuccessRegister, message);
            };
            _localBot.SendMessage(newSimpleUserId, $"/start {key}");
           
            var db = _serviceProvider.GetRequiredService<LiteDbContext>().Context;
            var subContext = db.GetCollection<Subscription>();
            Assert.True(subContext.Include(x => x.User).FindAll().Any(x => x.User.TelegramId == newSimpleUserId && x.State == SubscriptionState.Active), "No user in database");
        }

        [Fact]
        public void AllUsersMustReceiveNotify()
        {
            var constSite = "<a href=\"/selected/entry/145991\">" +
                            "<a href=\"/selected/entry/145992\">" +
                            "<a href=\"/selected/entry/145993\">";
            _localSnobServer.Answer = uri => constSite +
                                    "<a href=\"/selected/entry/145994\">" +
                                    "<a href=\"/selected/entry/145995\">";
            _checkJob.Execute();


            _localBot.SendMessage(_adminId, "/start");
            var useridList = new Dictionary<long, int>();
            useridList.Add(_adminId, 0);

            for (var i = 0; i < 50; i++)
            {
                var key = "";
                _localBot.ProcessReceiveMessage = (id, message) =>
                {
                    if (!message.Contains("tg"))
                    {
                        return;
                    }
                    var guidLength = Guid.Empty.ToString().Length;
                    key = message.Substring(message.Length - guidLength, guidLength);
                };
                _localBot.SendMessage(_adminId, "/share");
                _localBot.ProcessReceiveMessage = (id, message) =>
                {

                };
                var newSimpleUserId = _adminId + 1 + i;
                useridList.Add(newSimpleUserId, 0);
                _localBot.SendMessage(newSimpleUserId, $"/start {key}");
            }

            const string link1 = "/selected/entry/143994";
            const string link2 = "/selected/entry/143995";
            _localSnobServer.Answer = uri => constSite +
                                    $"<a href=\"{link1}\">" +
                                    $"<a href=\"{link2}\">";

            _localBot.ProcessReceiveMessage = (id, message) =>
            {
                if (message.Contains(link1) || message.Contains(link2))
                {
                    useridList[id]++;
                }
                else
                {
                    Assert.True(false, $"bot receive unknown message {message}");
                }
            };
            _checkJob.Execute();
            Assert.All(useridList, x => Assert.Equal(2, x.Value));
        }

        [Fact]
        public void AllUsersMustAddingRecursiveReceiveNotify()
        {
            const string constSite = "<a href=\"/selected/entry/145991\">" +
                                     "<a href=\"/selected/entry/145992\">" +
                                     "<a href=\"/selected/entry/145993\">";
            _localSnobServer.Answer = uri => constSite +
                                    "<a href=\"/selected/entry/145994\">" +
                                    "<a href=\"/selected/entry/145995\">";
            _checkJob.Execute();


            _localBot.SendMessage(_adminId, "/start");
            var useridList = new Dictionary<long, List<string>>
            {
                {
                    _adminId, new List<string>()
                }
            };

            for (var i = 0; i < 20; i++)
            {
                var key = "";
                _localBot.ProcessReceiveMessage = (id, message) =>
                {
                    if (!message.Contains("tg"))
                    {
                        return;
                    }
                    var guidLength = Guid.Empty.ToString().Length;
                    key = message.Substring(message.Length - guidLength, guidLength);
                };
                _localBot.SendMessage(_adminId + i, "/share");
                _localBot.ProcessReceiveMessage = (id, message) =>
                {

                };
                var newSimpleUserId = _adminId + 1 + i;
                useridList.Add(newSimpleUserId, new List<string>());
                _localBot.SendMessage(newSimpleUserId, $"/start {key}");
            }

            const string link1 = "/selected/entry/143994";
            const string link2 = "/selected/entry/143995";
            _localSnobServer.Answer = uri => constSite +
                                    $"<a href=\"{link1}\">" +
                                    $"<a href=\"{link2}\">";

            _localBot.ProcessReceiveMessage = (id, message) =>
            {
                if (message.Contains(link1) || message.Contains(link2))
                {
                    useridList[id].Add(message);
                }
                else
                {
                    Assert.True(false, $"bot receive unknown message {message}");
                }
            };
            _checkJob.Execute();

            var db = _serviceProvider.GetRequiredService<LiteDbContext>().Context;
            var keyContext = db.GetCollection<RegKey>();
            var keys = keyContext.Include(x => x.Author).Include(x => x.RegisteredUser).FindAll();
            var currentKey = keys.FirstOrDefault(x => x.Author.TelegramId == _adminId);
            
            const string foundKey = "found";
            useridList[_adminId].Add(foundKey);
            var index = 1;
            while (currentKey != null)
            {
                var registeredUser = currentKey.RegisteredUser;
                var childId = registeredUser.TelegramId;
                Assert.Equal(registeredUser.Generation, index++);
                useridList[childId].Add(foundKey);
                currentKey = keys.FirstOrDefault(x => x.Author.TelegramId == childId);
            }

            Assert.All(useridList, x =>
                {
                    var link1Exists = x.Value.Any(value => value.Contains(link1));
                    var link2Exists = x.Value.Any(value => value.Contains(link2));
                    var found = x.Value.Any(value => value.Contains(foundKey));
                    Assert.True(link1Exists && link2Exists && found);
                    Assert.Equal(3,x.Value.Count);
                }
            );
        }

        [Fact]
        public void UserNotReceiveMessageAfterBlock()
        {
            void BlockingSubscriptionAction()
            {
                _localSnobServer.Answer = uri => "<a href=\"/selected/entry/145990\">";
                var myreceiveMessage = false;
                _localBot.ProcessReceiveMessage = (id, message) =>
                {
                    myreceiveMessage = true;
                    throw new Telegram.Bot.Exceptions.ApiRequestException("Blocked", 403);
                };
                _checkJob.Execute();
                Assert.True(myreceiveMessage);
            }

            NormalWorkCheck(BlockingSubscriptionAction);
        }

        [Fact]
        public void UserNotReceiveMessageAfterUnsubscribe()
        {
            void BlockingSubscriptionAction()
            {
                _localBot.SendMessage(_adminId, "/stop");
                _localBot.ProcessReceiveMessage = (id, message) =>
                {
                    Assert.Equal(message, TeleMessages.StopSubscribtion);
                };
            }

            NormalWorkCheck(BlockingSubscriptionAction);
        }

        [Fact]
        public void BotMustSendCurrentReport()
        {
            _localBot.SendMessage(_adminId, "/start");
            var link = "/selected/entry/145991";
            var link2 = "/selected/entry/143391";
            _localSnobServer.Answer = uri => $"<a href=\"{link}\">" +
                                             $"<a href=\"{link2}\">";
            _checkJob.Execute();
            var receiveMessage = false;
            var resetevent = new ManualResetEvent(false);
            _localBot.ProcessReceiveMessage = (id, message) =>
            {
                Assert.Contains(link, message);
                Assert.Contains(link2, message);
                receiveMessage = true;
                resetevent.Set();
            };
            
            _localBot.SendMessage(_adminId, "/current");
            resetevent.WaitOne(32000);
            Assert.True(receiveMessage);
        }

        private void NormalWorkCheck(Action blockingSubscriptionAction)
        {
            _localBot.SendMessage(_adminId, "/start");
            _localSnobServer.Answer = uri => "<a href=\"/selected/entry/145991\">";
            var receiveMessage = false;
            _localBot.ProcessReceiveMessage = (id, message) =>
            {
                receiveMessage = true;
            };
            _checkJob.Execute();
            Assert.True(receiveMessage);

            blockingSubscriptionAction();

            _localSnobServer.Answer = uri => "<a href=\"/selected/entry/145992\">";
            receiveMessage = false;
            _localBot.ProcessReceiveMessage = (id, message) =>
            {
                receiveMessage = true;
            };
            _checkJob.Execute();
            Assert.False(receiveMessage);

            _localBot.SendMessage(_adminId, "/start");

            _localSnobServer.Answer = uri => "<a href=\"/selected/entry/145993\">";
            receiveMessage = false;
            _localBot.ProcessReceiveMessage = (id, message) =>
            {
                receiveMessage = true;
            };
            _checkJob.Execute();
            Assert.True(receiveMessage);
        }
    }
}
