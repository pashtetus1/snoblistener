﻿using System;
using System.IO;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace Pauk.SnobSpider.Test
{
    public class SimpleServer
    {
        public Func<Uri, string> Answer = uri => "404";

        private const int ServerStartPort = 7123;
        private static int _serverCurrentPort = ServerStartPort;
        public readonly string ServerUrl;

        public SimpleServer()
        {
            ServerUrl = $"http://localhost:{_serverCurrentPort++}/";
        }

        public void Start()
        {
            Task.Factory.StartNew(() =>
            {
                HttpListener listener = new HttpListener();
                listener.Prefixes.Add(ServerUrl);
                listener.Start();
                while (true)
                {
                    HttpListenerContext context = listener.GetContext();
                    Proccess(context);
                }
            }, TaskCreationOptions.LongRunning);
        }

        private void Proccess(HttpListenerContext context)
        {
            var request = context.Request;
            var response = context.Response;
            var prepareAnswer = Answer(request.Url);
            string responseStr = prepareAnswer;
            byte[] buffer = Encoding.UTF8.GetBytes(responseStr);
            response.ContentLength64 = buffer.Length;
            Stream output = response.OutputStream;
            output.Write(buffer, 0, buffer.Length);
            output.Close();
        }
    }
}
